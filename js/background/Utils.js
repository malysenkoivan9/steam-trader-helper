"use strict";

export const Utils = 
{
    getCookie: async function(name, domain = null) {
        const params = {name:name};
        if(domain != null)
            params.domain = domain;

        return new Promise((resolve, reject) => {
            chrome.cookies.getAll(params, function(cookies){
                resolve(cookies[0]);
            });
        });
    },

    sendAlertToUserUi: async function(text) {
        const tab = await getActiveTab();
        chrome.tabs.sendMessage(tab.id, {"action":"alert","text":"SteamTraderHelper: "+text});
    }
}

function getActiveTab() {
    return new Promise((resolve, reject) => {
        chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
            resolve(tabs[0]);
        });
    });
}